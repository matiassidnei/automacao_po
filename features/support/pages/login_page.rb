class LoginPage
    include Capybara::DSL

    def go
        visit "/index.php?controller=authentication&back=my-account"
    end

    def with(username, password)
        find('#email').set username
        find('#passwd').set password
        find(:xpath, "//a[@class='login']").click
    end

    def alert
        find(".alert-danger").text
    end
    # def username
    #     find('#email')
    # end
    
    # def password
    #     find('#passwd')
    # end

    # def login
    #     find(:xpath, "//a[@class='login']").click
    # end
end