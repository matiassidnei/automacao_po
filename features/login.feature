#language: pt

Funcionalidade: login
    Quero que o usuário consiga acessar o sistema com dados válidos e que ele seja barrado com dados inválidos

        @login
        Cenário: Acesso com sucesso
             Quando eu faco login com o usuario "matiassidnei@gmail.com" e senha "123456"
             Então eu acesso a área "My Account" com sucesso

        @login_branco
        Esquema do Cenário: Login invalido
             Quando eu faco login com o usuario <username> e senha <password>
             Então devo receber o alerta <texto>

        Exemplos:
                  | username  | password  | texto                       |
                  | "  "      | "asddas"  | "An email address required."|