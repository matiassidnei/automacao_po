
Quando('eu faco login com o usuario {string} e senha {string}') do |username, password|
    @login = LoginPage.new
    @login.go
    @login.with(username, password)
end
  
  Então('eu acesso a área {string} com sucesso') do |expect_name|
    expect(expect_name).to have_text expect_name
  end
     
  Então('devo receber o alerta {string}') do |expect_message| 
    sleep 5
    expect(@login.alert-danger).to eql expect_message
  end